<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quiz 2</title>
</head>

<body>
    <h1>Soal Essay A</h1>

    <?php 
       function hitung($string_data)
       {
           $operator = "";

           for($i=0; $i<strlen($string_data); $i++)
           {
               if(ord($string_data{$i})>=33 && ord($string_data{$i})<=47)
               {
                   $operator .= $string_data[$i];
               }
               else if(ord($string_data{$i})==58)
               {
                   $operator .= $string_data[$i];
               }
           }

           $str = explode($operator, $string_data);

           if($operator == "+")
           {
               return $str[0]+$str[1];
           }
           else if($operator == "-")
           {
                return $str[0]-$str[1];
           }
           else if($operator == "*")
           {
               return $str[0]*$str[1];
           }
           else if($operator == ":")
           {
               return $str[0]/$str[1];
           }
           else if($operator == "%")
           {
               return $str[0]%$str[1];
           }
           
       } 

       //Test case
       echo hitung("102*2") . "<br>";
       echo hitung("2+3") . "<br>";
       echo hitung("100:25") . "<br>";
       echo hitung("10%2") . "<br>";
       echo hitung("99-2");
    ?>
</body>

</html>