<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Class Animal</title>
</head>

<body>
    <h3>Release 0</h3>

    <?php
        require 'animal.php';
        $sheep = new Animal("shaun");

        echo $sheep->name . "<br>"; // "shaun"
        echo $sheep->legs . "<br>"; // 2
        echo $sheep->cold_blooded // false
    ?>

    <h3>Release 1</h3>

    <?php
        require 'ape.php';
        require 'frog.php';
        $sungokong = new Ape("kera sakti");
        $sungokong->yell(); // "Auooo"

        echo "<br>";
        
        $kodok = new Frog("buduk");
        $kodok->jump() ; // "hop hop"
    ?>
</body>

</html>